#include <bits/stdc++.h>
using namespace std;

struct node{
	int data;
	node *next;	
};


void ins(struct node**head , int n)
{
	node*temp = *head ;
	node *ptr = new node;
	if(*head==NULL)
	{
		*head = new node;
		(*head)->data = n;
		(*head)->next = NULL;
	}
	
	else
	{
		
		if(n<temp->data)
		{
			ptr = new node;
			ptr->data = n;
			ptr->next = *head;
			*head = ptr;
		}
			
		else
		{
			while(temp->next != NULL &&(n< temp->next->data && n>= temp->data))
			    temp=temp->next;
			
			if(temp->next == NULL) 
			{
				temp->next = new node;
				temp = temp->next;
				temp->data = n;
				temp->next = NULL;
			}
			else 
			{
				ptr = new node;
				ptr->data = n;
				ptr->next = temp->next; 
				temp->next = ptr;
			}
		}
	}
}

void display(struct node**head)
{
	struct node*temp=*head;
	while(temp!=NULL)
		{
			if(temp->next!=NULL)
			cout<<temp->data<<" ->";
			else
			cout<<temp->data;
			
			temp=temp->next; 
		}
}
int main()
{
	int i;
	struct node *head = NULL;
	int arr[5]={2,5,7,10,15};
	for(i=0;i<5;i++)
	    ins(&head,arr[i]);
	ins(&head,9);
	display(&head);
	return 0;
}
